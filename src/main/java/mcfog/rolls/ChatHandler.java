package mcfog.rolls;

import mcfog.rolls.util.RollsHandler;
import net.minecraft.client.Minecraft;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


public class ChatHandler {

    @SubscribeEvent
    public void onServerChatEvent(ServerChatEvent event) {

        System.out.println("onServerChatEvent");

        handleServerRollCommand(event);
    }

    private void handleServerRollCommand(ServerChatEvent event) {

        String message = event.getMessage();
        String username = event.getUsername();
        String result = "";

        if (RollsHandler.hasRollCommand(message)) {
            System.out.println(username + ": " + message);

            // cancel the vanilla chat
            event.setCanceled(true);

            result = RollsHandler.handle(message, username);

            RollsHandler.sendMessageToWorld(result);
        }
    }

    // maybe best way to check if is multiplayer game
    private boolean isMutliplayer() {

        boolean isSingleplayer, isMultiplayer;

        try {
            isSingleplayer = (Minecraft.getMinecraft() != null);
        } catch (NoClassDefFoundError e1) {
            isSingleplayer = false;
        }
        isMultiplayer = ! isSingleplayer;

        System.out.println("Mult: " + isMultiplayer);

        return isMultiplayer;
    }
}
