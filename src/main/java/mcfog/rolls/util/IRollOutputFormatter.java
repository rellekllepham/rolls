package mcfog.rolls.util;

import java.util.ArrayList;

// For custom output formatting
public interface IRollOutputFormatter {
    String format(ArrayList<Integer> results, String sender);
}
