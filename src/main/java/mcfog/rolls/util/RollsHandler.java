package mcfog.rolls.util;

import mcfog.rollsystem.Rollsystem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import java.util.ArrayList;
import java.util.List;

public class RollsHandler {

    public static IRollOutputFormatter DefaultRollOutputFormatter = new IRollOutputFormatter() {
        @Override
        public String format(ArrayList<Integer> results, String sender) {
            String formattedResults;
            if (results.isEmpty()) {
                formattedResults = "Invalid inputs";
            } else {
                if (results.size() < 2) {
                    formattedResults = "Rollsystem Lib parse error";
                } else {
                    formattedResults = "Player " + sender + " "
                            + "rolls " + itoa(results.get(1)) + " "
                            + "from first value " + itoa(results.get(0)) + " "
                            + "with modificators "
                            + modificatorsFormat(results);
                }
            }

            System.out.println("formattedResult: " + formattedResults);

            return formattedResults;
        }
    };


    // main handle method
    public static String handle(String message, String sender) {

        String result = DefaultRollOutputFormatter.format(fetchRollResults(message), sender);

        return result;
    }

    // main handle method with custom output formatter
    public static String handle(String message, String sender, IRollOutputFormatter formatter) {

        String result = formatter.format(fetchRollResults(message), sender);

        return result;
    }


    private static ArrayList<Integer> fetchRollResults(String message) {
        // fetch the results
        ArrayList<Integer> results;
        results = Rollsystem.Roll(message);
        System.out.println("results: " + results);

        return  results;
    }


    public static void sendMessageToWorld(String message){

        for (EntityPlayerMP receiver : getPlayerList()) {
            receiver.sendMessage(new TextComponentString(message));
        }

    }

    // fetch MP players
    private static List<EntityPlayerMP> getPlayerList(){

        List<EntityPlayerMP> players;

        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();

        players = server.getPlayerList().getPlayers();


        return players;
    }

    // check if chat message starts with fudge "%"
    public static boolean hasRollCommand(String message) {

            if (message.startsWith("%")){
                return true;
            }

        return false;
    }

//
//    // format result message for MP
//    public static String formattedOutputMP(ArrayList<Integer> results, String sender) {
//        String formattedResults;
//        if (results.isEmpty()) {
//            formattedResults = "Invalid inputs";
//        } else {
//            if (results.size() < 2) {
//                formattedResults = "Rollsystem Lib parse error";
//            } else {
//                formattedResults = "Player " + sender + " "
//                        + "rolls " + itoa(results.get(1)) + " "
//                        + "from first value " + itoa(results.get(0)) + " "
//                        + "with modificators "
//                        + modificatorsFormat(results);
//            }
//        }
//
//        System.out.println("formattedResult: " + formattedResults);
//
//        return formattedResults;
//    }

    // format modificators
    private static String modificatorsFormat(ArrayList<Integer> list) {
        int startIndex = 2;
        String formatted = "[";

        if (startIndex < list.size()) {
            for (int i = startIndex; i < list.size() - 1; i++) {
                formatted += itoa(list.get(i)) + " ";
            }
            formatted += itoa(list.get(list.size() - 1));
        }
        formatted += "]";

        return formatted;
    }

    private static String itoa(int val) {
        return Integer.toString(val);
    }

}
