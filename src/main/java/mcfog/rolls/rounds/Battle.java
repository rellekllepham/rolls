package mcfog.rolls.rounds;

public class Battle {

    // Unique Battle ID
    private String ID;

    public Battle(String ID) {
        this.ID = ID;
    }

    // Get Battle ID
    public String ID(){
        return this.ID;
    }

    // `/btl create`
    public void create() {

    }

    // `/btl stop`
    public void stop() {

    }

    // `/btl add <username>`
    public void addUser(String user){

    }

    // `/btl add <username1> <username2> <username3> ...`
    public void addUser(String[] users){

    }

    // `/btl ls`
    public void showUserList(){

    }

    // `/btl chreact <username> <number>`
    public void setUserReaction(String user, float reaction){

    }

    // /btl setreact <number1> <number2> <number3> ...
    public void setReactionsForUserList(float[] reactions){

    }

    // `/btl neworder`
    public void makeOrder() {

    }

    // `/btl nextround`
    public void nextRound() {

    }

    // `/btl lsorder`
    public void showOrderList() {

    }

    // `/btl curr`
    public void showCurrentUserOrder() {

    }

    // `/btl del <username>`
    public void deleteUser(String user){

    }

    // `/btl del <username1> <username2> <username3> ...`
    public void deleteUser(String[] users) {

    }

    // `/btl save <battle_name>`
    public void save(String battlename) {

    }

    // `/btl load <battle_name>`
    public void load(String battlename) {

    }


}
