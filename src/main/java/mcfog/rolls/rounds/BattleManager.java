package mcfog.rolls.rounds;

import mcfog.rolls.util.UUIDGenerator;

import java.util.HashMap;
import java.util.Map;

public class BattleManager {

    // <ID, Battle>
    private HashMap<String, Battle> battles;

    public BattleManager(){
        battles = new HashMap<>();
    }

    public Battle createBattle(){
        String ID = UUIDGenerator.getUUID();
        Battle btl = new Battle(ID);
        battles.put(ID, btl);

        return btl;
    }

    public void deleteBattle(String ID) {
        for (Map.Entry<String, Battle> entry: battles.entrySet()) {
            String btlID = entry.getKey();
            Battle btl = entry.getValue();

            if (ID.equals(btlID)) {
                btl.stop();
                battles.remove(btlID);

                return;
            }
        }
    }

    public void deleteBattle(Battle btl) {
        if (battles.containsValue(btl)) {
            battles.remove(btl.ID());
        }
    }

    public void saveBattle(String battlename) {

    }

    public void loadBattle(String battlename) {

    }

}
