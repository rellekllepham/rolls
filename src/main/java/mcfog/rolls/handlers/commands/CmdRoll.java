package mcfog.rolls.handlers.commands;

import joptsimple.internal.Strings;
import mcfog.rollsystem.Rollsystem;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class CmdRoll implements ICommand {

    private final List aliases;
    private String CmdName;
    private String CmdUsage;
    private ArrayList<Integer> results;

    public CmdRoll(){
        CmdName = "CmdRoll";
        CmdUsage = "roll <any>";
        aliases = new ArrayList();
        aliases.add("roll");
        aliases.add("r");
        aliases.add("%");
    }


    @Override
    public String getName() {
        return CmdName;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return CmdUsage;
    }

    @Override
    public List<String> getAliases() {
        return this.aliases;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {



        System.out.println("command:" + CmdName);
        System.out.println("args:" + Strings.join(args, ","));

        // fetch the results
        results = Rollsystem.Roll(Strings.join(args, " "));
        System.out.println("results: " + results);

        String formattedResult = formattedOutput(results, sender);
        System.out.println("formattedResult: " + formattedResult);

        sendMessageToWorld(formattedResult);

    }

    private String formattedOutput(ArrayList<Integer> results, ICommandSender sender) {
        String formattedResults;
        if (results.isEmpty()) {
            formattedResults = "Invalid inputs";
        } else {
            if (results.size() < 2) {
                formattedResults = "Rollsystem Lib parse error";
            } else {
                formattedResults = "Player " + sender.getName() + " "
                        + "rolls " + itoa(results.get(1)) + " "
                        + "from first value " + itoa(results.get(0)) + " "
                        + "with modificators "
                        + modificatorsFormat(results);
            }
        }

        return formattedResults;
    }

    private void sendMessageToWorld(String text) {

        List<EntityPlayerMP> players = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers();

        for (EntityPlayerMP receiver : players) {
            receiver.sendMessage(new TextComponentString(text));
        }

        //sender.sendMessage(new TextComponentString(text));
    }

    private String modificatorsFormat(ArrayList<Integer> list) {
        int startIndex = 2;
        String formatted = "[";

        if (startIndex < list.size()) {
            for (int i = startIndex; i < list.size() - 1; i++) {
                formatted += itoa(list.get(i)) + " ";
            }
            formatted += itoa(list.get(list.size() - 1));
        }
        formatted += "]";

        return formatted;
    }

    private String itoa(int val) {
        return Integer.toString(val);
    }

    private String utf8(String text) {
        String result;
        result = text;
        return result;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public int compareTo(ICommand o) {
        return 0;
    }
}
