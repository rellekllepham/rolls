package mcfog.rollsystem;

import java.util.ArrayList;

public class Rollsystem {

    public static ArrayList Roll(String chatmessage) {

        ArrayList<Integer> returnes = new ArrayList<Integer>();

        if (chatmessage.matches("(^%)(\\d+)")) // Проверка соответствия сообщения шаблону %[число].
        {
            String startnumberstring = chatmessage.replaceAll("%", ""); // Убираем символ процента из значения
            int bottom_line = 1; // Нижняя граница бросков
            int random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску

            int modificator1;


            switch (random_number) {
                case 1:
                    modificator1 = -1;
                    break;
                case 2:
                    modificator1 = 0;
                    break;
                case 3:
                    modificator1 = 1;
                    break;
                default:
                    modificator1 = 0;
                    break;

            }

            random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску

            int modificator2;


            switch (random_number) {
                case 1:
                    modificator2 = -1;
                    break;
                case 2:
                    modificator2 = 0;
                    break;
                case 3:
                    modificator2 = 1;
                    break;
                default:
                    modificator2 = 0;
                    break;

            }

            random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску

            int modificator3;


            switch (random_number) {
                case 1:
                    modificator3 = -1;
                    break;
                case 2:
                    modificator3 = 0;
                    break;
                case 3:
                    modificator3 = 1;
                    break;
                default:
                    modificator3 = 0;
                    break;

            }

            int startnumberint = Integer.parseInt(startnumberstring); // Преобразовываем начальное значение из String в int
            int result = startnumberint + modificator1 + modificator2 + modificator3; // Формирование результата

            returnes.add(startnumberint);
            returnes.add(result);
            returnes.add(modificator1);
            returnes.add(modificator2);
            returnes.add(modificator3);
            return returnes;

        } else {
            if (chatmessage.matches("(^%)([-])(\\d+)")) // Проверка соответствия сообщения шаблону %[отрицательное число].
            {
                String chatmessage1 = chatmessage.replaceAll("%", ""); // Убираем символ процента из значения
                String startnumberstring = chatmessage1.replaceAll("-", ""); // Убираем минус из значения

                int bottom_line = 1; // Нижняя граница бросков
                int random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску
                int modificator1;

                switch (random_number) {
                    case 1:
                        modificator1 = -1;
                        break;
                    case 2:
                        modificator1 = 0;
                        break;
                    case 3:
                        modificator1 = 1;
                        break;
                    default:
                        modificator1 = 0;
                        break;

                }

                random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску

                int modificator2;

                switch (random_number) {
                    case 1:
                        modificator2 = -1;
                        break;
                    case 2:
                        modificator2 = 0;
                        break;
                    case 3:
                        modificator2 = 1;
                        break;
                    default:
                        modificator2 = 0;
                        break;

                }

                random_number = bottom_line + (int) (Math.random() * 3); // Генерация модификатора к броску

                int modificator3;

                switch (random_number) {
                    case 1:
                        modificator3 = -1;
                        break;
                    case 2:
                        modificator3 = 0;
                        break;
                    case 3:
                        modificator3 = 1;
                        break;
                    default:
                        modificator3 = 0;
                        break;

                }

                int startnumberint = Integer.parseInt(startnumberstring); // Преобразовываем начальное значение из String в int
                int minusstartnumberint = startnumberint * -1; // Делаем отрицательное начальное значение
                int result = minusstartnumberint + modificator1 + modificator2 + modificator3; // Формирование результата

                returnes.add(minusstartnumberint);
                returnes.add(result);
                returnes.add(modificator1);
                returnes.add(modificator2);
                returnes.add(modificator3);
                return returnes;

            } else {

                return returnes;
            }
        }
    }
}
